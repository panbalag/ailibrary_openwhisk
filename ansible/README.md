# Ansible

This library contains ansible roles to cleanup and deploy OpenWhisk actions that trigger machine learning models via jobs and poll the status of such jobs. The scripts create the OpenWhisk package 'ai-library' and the following actions under this package,

1. ai-library/poll-status
2. ai-library/associative-rule-learning
3. ai-library/correlation-analysis
4. ai-library/flake-analysis-training
5. ai-library/flake-analysis-prediction
6. ai-library/sentiment-analysis
7. ai-library/duplicate-bug-detection-training
8. ai-library/duplicate-bug-detection-prediction
9. ai-library/matrix-factorization

# Ansible Scripts

## roles/deploy_actions

`deploy.yaml` - File to activate role 'deploy_actions'

## roles/deploy_actions/tasks

`cleanup_actions.yml` - clean up the above package and associated actions.

`deploy_actions.yml` - deploy the above package and associated actions.

## roles/deploy_actions/defaults

`main.yml` - parameter definition for commands in deploy_actions.yml

# Command

## Parameters

1. openshift_cluster - endpoint to OpenShift Server
2. openwhisk_project - namespace under which OpenWhisk is installed
3. openshift_project - namespace to use to execute jobs
4. s3Endpoint - object storage endpoint
5. s3AccessKey - access key for object storage
6. s3SecretKey - secret key for object storage
7. s3Bucket - bucket in object storage where all data are stored.

## Steps

The following steps deploy OpenWhisk actions for all the machine learning models implemented in this library.

1. cd ansible
2. ansible-playbook roles/deploy_actions/deploy.yml --extra-vars "openshift_cluster=[ ] openwhisk_project=[ ] openshift_project=[ ] s3Endpoint=[ ] s3AccessKey=[ ] s3SecretKey=[ ] s3Bucket=[ ]"


# Linear Regression

Linear regression is used to establish the relationship between a dependent variable and one (simple linear regression) or more independent variables (multivariate regression) using a best fit straight line.

## Contents

`regression.py` - linear regression model (simple or multivariate).
##### Parameters
* s3Path - location containing training and prediction data set (location in the S3 bucket).
* s3Destination - location where results are stored (location in the s3 bucket)
* training - name of the training data set (csv file with top row being column names).
* prediction - name of the prediction data set (same csv format as above, exclude the column names).

## Workflow

### Save Data

#### Training Data

The following example shows sample s3Path that points to the folder where training data is stored.

    s3Path - risk_analysis/
    Files -
        training.csv
	prediction.csv

The following example shows what an individual training data looks like. 

    training.csv
       S0,S1,S2,S3,S4,S5,S6,S7,S8,S9,S10,Risk
       0,0,1,0,0,0,0,0,0,0,0.8
       0,0,1,0,0,0,1,1,0,0,0.5
       ..
       ..

#### Prediction Data

The following example shows sample s3Path that points to the folder where prediction data is stored.

    s3Path - /DH-DEV-DATA/cchase/flake/prediction-data/failures/records
    Files -
	prediction.csv

	bug_978041.json
          0,0,1,0,0,1,0,0,0,0,0
          1,0,1,0,0,0,1,1,0,0,0

### Run Model

#### Training and Prediction (run in the same code)

    curl -u user:secret \
    "https://openwhisk-dh-prod-ow.cloud.openshift.com/api/v1/namespaces/_/actions/ai-library/linear-regression?" \
    -X POST -H "Content-Type: application/json" -d \
    '{ "name"    : "linear-regression", 
       "app_args": "-s3Path=risk_analysis/ -training=training.csv -prediction=prediction.csv"}'
  
#### Poll Status

    curl -u user:secret \
    "https://openwhisk-dh-prod-ow.openshift.com/api/v1/namespaces/_/actions/ai-library/poll-status?" \
    -X POST -H "Content-Type: application/json" -d \
    '{ "name"               : "linear-regression"}'

### Use Results

#### Prediction

The following example shows sample s3Destination that points to the folder where prediction results are stored.

    s3Destination - risk_analysis/results
    Files -
        results.txt

    results.txt contains json data with predicted values and accuracy measures Mean Absolute Error (MAE) and Median Absolute Error (MdAE).

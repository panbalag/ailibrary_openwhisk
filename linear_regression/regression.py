import pandas as pd
import json
from pandas import DataFrame
from sklearn import linear_model
import csv
import argparse
import os
from os import listdir
from os.path import isfile, join
from sklearn.metrics import mean_squared_error
from math import sqrt
import statistics
import inspect
import sys
currentdir = os.path.dirname(
               os.path.abspath(
                inspect.getfile(inspect.currentframe())
                )
               )
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir + "/accuracy_measures")
import measures
sys.path.append(parentdir + "/storage")
import s3

parser = argparse.ArgumentParser()
parser.add_argument('-s3Path', help='path to source folder', default='')
parser.add_argument('-s3endpointUrl', help='s3 endpoint url', default='')
parser.add_argument('-s3objectStoreLocation', help='s3 bucket', default='')
parser.add_argument('-s3accessKey', help='s3 access key', default='')
parser.add_argument('-s3secretKey', help='s3 secret key', default='')
parser.add_argument('-s3Destination', help='path to store results', default='')
parser.add_argument('-training', help='training data set', default='')
parser.add_argument('-prediction', help='prediction data set', default='')
args = parser.parse_args()

if not os.path.exists('tmp/data/'):
    os.makedirs('tmp/data/')

s3.download_folder(args.s3accessKey,
                   args.s3secretKey,
                   args.s3endpointUrl,
                   args.s3objectStoreLocation,
                   args.s3Path,
                   'tmp/data/')

if not os.path.exists('tmp/result/'):
    os.makedirs('tmp/result/')

mypath = 'tmp/data/'


TRAINING_DATA = mypath + args.training

with open(TRAINING_DATA, "rt") as f:
 reader = csv.reader(f)
 i = next(reader)

 col = i[:]
 y_col = i[-1]
 del i[-1]
 x_col = i

data = pd.read_csv(TRAINING_DATA)
df = DataFrame(data,columns=col)

X = df[x_col]
Y = df[y_col]
 
# with sklearn
regr = linear_model.LinearRegression()
regr.fit(X, Y)

print('=================\nModel Parameters:\n=================\n')
print('----------\nIntercept:\n----------\n', regr.intercept_)
print('-------------\nCoefficients:\n-------------\n', regr.coef_)

# prediction with sklearn
y_true = []
y_pred = []
abs_error=[]
f = open(mypath + args.prediction, "r")
lines = f.readlines()
print('\n===========\nPrediction:\n===========\n')
print('  Predicted   vs  Actual\n')

outfile = open('tmp/result/result.txt', 'a')
output = {}
for item in lines:
 val = item.split(',')
 expected = val[-1]
 del val[-1]
 results = list(map(float, val))
 predicted = regr.predict([results])
 y_true.append(float(expected))
 y_pred.append(float(predicted))
 print(float(predicted), ',',float(expected))

output['predicted'] = y_pred
output['expected'] =  y_true

print('=========\nAccuracy: \n=========\n')
mae = measures.predictive_accuracy(y_true,y_pred,"MAE")
mdae = measures.predictive_accuracy(y_true,y_pred,"MdAE")
output['mae'] = mae
output['mdae'] = mdae
json.dump(output, outfile)
outfile.close()
print(measures.predictive_accuracy(y_true,y_pred,"MAE"))
print(measures.predictive_accuracy(y_true,y_pred,"MdAE"))

# Write results to Ceph backend
s3.upload_folder(args.s3accessKey,
                 args.s3secretKey,
                 args.s3endpointUrl,
                 args.s3objectStoreLocation,
                 'tmp/result',
                 args.s3Destination)


# Storage

This library contains routines to calculate the following measures of prediction accuracy.

# Methods

`Mean Absolute Error (MAE)`

`Median Absolute Error (MdAE)`

`Mean Square Error (MSE)`

`Median Square Error (MdSE)`

`Root Mean Square Error (RMSE)`

`Root Median Square Error (RMdSE)`

`Mean Absolute Percentage Error (MAPE)`

`Median Absolute Percentage Error (MdAPE)`

`Symmetric Mean Absolute Percentage Error (SMAPE)`

`Symmetric Median Absolute Percentage Error (SMdAPE)`

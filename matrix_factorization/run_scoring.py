"""The Endpoint to serve model training and scoring."""
import os
from os import listdir
from os.path import isfile, join
import json
import argparse
import sys
import inspect
currentdir = os.path.dirname(
               os.path.abspath(
                inspect.getfile(inspect.currentframe())
                )
               )
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir + "/storage")
import s3
import time

LOCAL_DATA_LINK = 'matrix_factorization/tmp/ai_models/hpf-insights'

global scoring_status
global scoring_object


parser = argparse.ArgumentParser()
parser.add_argument('-s3Path', help='path to prediction data', default='')
parser.add_argument('-model', help='path to trained model', default='')
parser.add_argument('-config', help='path to config file', default='')
parser.add_argument('-s3endpointUrl', help='s3 endpoint url', default='')
parser.add_argument('-s3objectStoreLocation', help='s3 bucket', default='')
parser.add_argument('-s3accessKey', help='s3 access key', default='')
parser.add_argument('-s3secretKey', help='s3 secret key', default='')
parser.add_argument('-s3Destination', help='path to store results', default='')
args = parser.parse_args()

# Create S3 session to access Ceph backend and get an S3 resource
session = s3.create_session_and_resource(args.s3accessKey,
                                         args.s3secretKey,
                                         args.s3endpointUrl)

# Download the config file that contains hyper parameter definition
s3.download_file(session,
                 args.s3objectStoreLocation,
                 args.config,
                 'matrix_factorization/src/config.py')

from src.data_store.local_data_store import LocalDataStore
from src.scoring.hpf_scoring import HPFScoring
from src.utils import convert_string2bool_env
from src.config import (HPF_SCORING_REGION)
global scoring_status
global scoring_object

SOURCE_DIR = LOCAL_DATA_LINK + '/' + HPF_SCORING_REGION


if not os.path.exists(SOURCE_DIR):
    os.makedirs(SOURCE_DIR)

s3.download_folder(args.s3accessKey,
                   args.s3secretKey,
                   args.s3endpointUrl,
                   args.s3objectStoreLocation,
                   args.model,
                   SOURCE_DIR)

INPUT_DIR = 'matrix_factorization/tmp/input'
if not os.path.exists(INPUT_DIR):
    os.makedirs(INPUT_DIR)

s3.download_folder(args.s3accessKey,
                   args.s3secretKey,
                   args.s3endpointUrl,
                   args.s3objectStoreLocation,
                   args.s3Path,
                   INPUT_DIR)

if not os.path.exists('matrix_factorization/tmp/result/'):
    os.makedirs('matrix_factorization/tmp/result/')

# Run hpf scoring model on the input to find the package
# recommendation

def hpf_scoring(input_stack):

    output_json = dict()
    if input_stack["ecosystem"] != HPF_SCORING_REGION: 
        output_json = {"Error": "Input ecosystem does not match"}
    else:
        companion_recommendation, package_to_topic_dict, \
            missing_packages = scoring_object.predict(
                input_stack['package_list'])
        output_json = {
            "alternate_packages": {},
            "missing_packages": missing_packages,
            "companion_packages": companion_recommendation,
            "ecosystem": input_stack["ecosystem"],
            "package_to_topic_dict": package_to_topic_dict,
        }
    return output_json


if __name__ == "__main__":
 
    if HPF_SCORING_REGION != "":  
        data_object = LocalDataStore(LOCAL_DATA_LINK)
        scoring_object = HPFScoring(datastore=data_object)
        scr_object = HPFScoring(datastore=data_object)
        data = []
        listfiles = [f for f in listdir(INPUT_DIR) if isfile(join(INPUT_DIR, f))]
        for fl in listfiles:
            print("Processing input - " + fl)
            fp = open(INPUT_DIR + '/' +fl, "r")
            data = json.loads(fp.read())
            response = hpf_scoring(data)
            with open('matrix_factorization/tmp/result/' + fl,"w") as f:
                json.dump(response, f)
        # Write results to Ceph backend
        s3.upload_folder(args.s3accessKey,
                         args.s3secretKey,
                         args.s3endpointUrl,
                         args.s3objectStoreLocation,
                         'matrix_factorization/tmp/result',
                         args.s3Destination)
    else:
        print("Error: No scoring region provided")

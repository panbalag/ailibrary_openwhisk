import model_topics as mt
import os
from os import listdir
from os.path import isfile, join
import argparse
import time
import json
import sys
import inspect
currentdir = os.path.dirname(
               os.path.abspath(
                inspect.getfile(inspect.currentframe())
                )
               )
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir + "/storage")
import s3


NUM_TOPICS = 2
NUM_WORDS = 10
PASSES = 50

parser = argparse.ArgumentParser()
parser.add_argument('-s3Path', help='path to training data', default='')
parser.add_argument('-s3endpointUrl', help='s3 endpoint url', default='')
parser.add_argument('-s3objectStoreLocation', help='s3 bucket', default='')
parser.add_argument('-s3accessKey', help='s3 access key', default='')
parser.add_argument('-s3secretKey', help='s3 secret key', default='')
parser.add_argument('-s3Destination', help='path to store results', default='')
args = parser.parse_args()

# Create S3 session to access Ceph backend and get an S3 resource
session = s3.create_session_and_resource(args.s3accessKey,
                                         args.s3secretKey,
                                         args.s3endpointUrl)
objects = s3.get_objects(session, args.s3objectStoreLocation, args.s3Path)

filelist = []

# For each existing bug, perform topic modeling of the contents
# to arrive at topics for each bug.
for key in objects:
    doc_complete = []
    data = {}
    obj = session.Object(args.s3objectStoreLocation, key)
    contents = obj.get()['Body'].read().decode('utf-8')
    path = key.split("/")
    filename = path[-1]
    if contents:
        jcontents = json.loads(contents)
        title = jcontents['title']
        comments = jcontents['content']
        doc_complete = comments.split('\n')
        doc_clean = [mt.clean(doc).split() for doc in doc_complete]
        topics = mt.gen_topics(doc_clean, NUM_TOPICS, NUM_WORDS, PASSES)
        data['title'] = title
        data['content'] = topics
        if not os.path.exists('tmp'):
            os.makedirs('tmp')
        filelist.append(filename)
        with open('tmp/' + filename, 'w') as outfile:
            json.dump(data, outfile)

s3.upload_folder(args.s3accessKey,
                 args.s3secretKey,
                 args.s3endpointUrl,
                 args.s3objectStoreLocation,
                 'tmp',
                 args.s3Destination)

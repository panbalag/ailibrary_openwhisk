import numpy as np
import re
import os
from itertools import islice
from operator import itemgetter
from sklearn.metrics import jaccard_similarity_score
from os import listdir
from os.path import isfile, join
import nltk
nltk.download('stopwords')
nltk.download('wordnet')
nltk.download('punkt')
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import argparse
import json
import model_topics as mt
import time
import sys
import inspect
currentdir = os.path.dirname(
               os.path.abspath(
                inspect.getfile(inspect.currentframe())
                )
               )
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir + "/storage")
import s3


NUM_TOPICS = 2
NUM_WORDS = 10
PASSES = 50

# Find distance between given set of words

def jaccard_similarity(x, y):
    x1 = set(x)
    y1 = set(y)
    z = x1.intersection(y1)
    return ((len(z)*100)/(len(x1) + len(y1) - len(z)))


# Tokenize given text

def get_tokens(text):
    stop_words = set(stopwords.words('english'))
    word_tokens = word_tokenize(text)
    filtered_sentence = [w for w in word_tokens if not w in stop_words]
    filtered_sentence = []
    for w in word_tokens:
        if w not in stop_words:
            filtered_sentence.append(w.lower())
    return(filtered_sentence)


parser = argparse.ArgumentParser()
parser.add_argument('-s3Path', help='path to trained topics', default='')
parser.add_argument('-s3endpointUrl', help='s3 endpoint url', default='')
parser.add_argument('-s3objectStoreLocation', help='s3 bucket', default='')
parser.add_argument('-s3accessKey', help='s3 access key', default='')
parser.add_argument('-s3secretKey', help='s3 secret key', default='')
parser.add_argument('-s3Destination', help='path to store results', default='')
parser.add_argument('-bugs', help='bugs to predict matches', default='')
parser.add_argument('-num_matches', help='Number of matches', default=5)
args = parser.parse_args()

if not os.path.exists('tmp/duplicates/'):
    os.makedirs('tmp/duplicates/')

s3.download_folder(args.s3accessKey,
                   args.s3secretKey,
                   args.s3endpointUrl,
                   args.s3objectStoreLocation,
                   args.bugs,
                   'tmp/duplicates/')

if not os.path.exists('tmp/result/'):
    os.makedirs('tmp/result/')

mypath = 'tmp/duplicates/'
onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]

# For each given bug, compute the jaccard similarity of the title
# and contents of existing bug. Collect all scores and sort to 
# find the top matches

for duplfile in onlyfiles:
    print("Processing - " + duplfile)
    filepath = mypath + duplfile
    with open(filepath) as f:
        data = json.load(f)
        print(filepath)
        dtitle = data['title']
        dcontent = data['content']

        title = re.sub(r"[^a-zA-Z0-9]+", ' ', dtitle)
        content = dcontent.split("\n")
        for index, item in enumerate(content):
            content[index] = re.sub(r"[^a-zA-Z0-9]+", ' ', item)

        doc_clean = [mt.clean(doc).split() for doc in content]
        bcontents = mt.gen_topics(doc_clean, NUM_TOPICS, NUM_WORDS, PASSES)

        session = s3.create_session_and_resource(args.s3accessKey,
                                                   args.s3secretKey,
                                                   args.s3endpointUrl)
        objects = s3.get_objects(session, args.s3objectStoreLocation, args.s3Path)
        filelist = []
        tscores = []
        cscores = []
        for key in objects:
            doc_complete = []
            data = {}
            obj = ""
            obj = session.Object(args.s3objectStoreLocation, key)

            contents = obj.get()['Body'].read().decode('utf-8')
            filename = key.split("/")
            if contents:
                jcontents = json.loads(contents)

                jtitle = jcontents['title']
                jcontent = jcontents['content']

                # Match by title
                x = get_tokens(title)
                y = get_tokens(jtitle)
                score = jaccard_similarity(x, y)
                tscore = []
                tscore.append(score)
                tscore.append(filename[-1])
                tscore.append(jtitle)
                tscores.append(tscore)

                # Match by content
                for item in jcontent:
                    data = re.sub(r"[^a-zA-Z0-9]+", ' ', item)
                    y = get_tokens(data)
                    for cnt in bcontents:
                        x = get_tokens(cnt)
                        score = jaccard_similarity(x, y)
                        cscore = []
                        cscore.append(score)
                        cscore.append(filename[-1])
                        cscore.append(jtitle)
                        cscores.append(cscore)

        matches = {}
        match = {}
        index = 0
        tscores = sorted(tscores, key=itemgetter(0), reverse=True)
        del tscores[args.num_matches:]
        for item in tscores:
            match = {}
            match['id'] = item[1]
            match['title'] = item[2]
            matches[index] = match
            index += 1

        cscores = sorted(cscores, key=itemgetter(0), reverse=True)
        del cscores[args.num_matches:]
        for item in cscores:
            match = {}
            match['id'] = item[1]
            match['title'] = item[2]
            matches[index] = match
            index += 1

        with open('tmp/result/' + duplfile, 'w') as outfile:
            json.dump(matches, outfile)


# Write results to Ceph backend
s3.upload_folder(args.s3accessKey,
                 args.s3secretKey,
                 args.s3endpointUrl,
                 args.s3objectStoreLocation,
                 'tmp/result',
                 args.s3Destination)

# OpenWhisk Service for ML Model Workflow

This repository contains OpenWhisk actions that can be used to execute Machine Learning models as well as poll the status on such tasks.

# Actions

`actions/service.py`  Trigger openshift job to execute machine learning models.

`actions/poll_job.py` Trigger openshift job to poll the status of a given job.


# Deployment

## Parameters

1. openshift_cluster - endpoint to OpenShift Server
2. openwhisk_project - namespace under which OpenWhisk is installed
3. openshift_project - namespace to use to execute jobs
4. s3Endpoint - object storage endpoint
5. s3AccessKey - access key for object storage
6. s3SecretKey - secret key for object storage
7. s3Bucket - bucket in object storage where all data are stored.

## Steps

The following steps deploy OpenWhisk actions for all the machine learning models implemented in this library.

1. cd ansible
2. ansible-playbook roles/deploy_actions/deploy.yml --extra-vars "openshift_cluster=[ ] openwhisk_project=[ ] openshift_project=[ ] s3Endpoint=[ ] s3AccessKey=[ ] s3SecretKey=[ ] s3Bucket=[ ]"


import os
import requests
import json
import time


def main(args):
    # HTTP request endpoint and token information.
    server = args['server']
    namespace = args['namespace']
    jobname = args['name']
    url_job = server + "/apis/batch/v1/namespaces/" + namespace + "/jobs/" + jobname
    token = open('/var/run/secrets/kubernetes.io/serviceaccount/token').read()

    # Header information for HTTP Request
    auth = "Bearer " + token
    headers = {"Authorization": auth, "Accept": "application/json",
               "Connection": "close", "Content-Type": "application/json"}

    # Initiate HTTP request and store response
    response = requests.get(url_job, headers=headers, verify=False)

    # Convert response to JSON format and retrieve status
    response_json = response.json()
    status = response_json['status']
    return {"status": status}

import os
import requests
import json
import time


def main(args):
    # Openshift server name
    server = args['server']

    # Token for HTTP request authentication
    token = open('/var/run/secrets/kubernetes.io/serviceaccount/token').read()
    
    # The following parameters will be used in the payload section of
    # Image Stream, BuildConfig and Job configuration.
    name = args['name']
    namespace = args['namespace']

    #Create endpoints for Image Stream, BuildConfig and Job HTTP requests
    url_imagestream =  server + "/apis/image.openshift.io/v1/namespaces/" + namespace + "/imagestreams"
    url_buildconfig =  server + "/apis/build.openshift.io/v1/namespaces/" + namespace + "/buildconfigs"
    url_job = server + "/apis/batch/v1/namespaces/" + namespace + "/jobs"


    # BuildConfig specific arguments
    app_file = args['app_file']
    buildconfig_image = args['docker_image']
    buildconfig_cpu = args['buildconfig_cpu']
    buildconfig_memory = args['buildconfig_memory']
    buildconfig_git_url = args['buildconfig_git_url']

    # Job specific arguments
    s3accesskey = args['s3AccessKey']
    s3secretkey = args['s3SecretKey']
    s3endpoint = args['s3Endpoint']
    s3objectstorelocation = args['s3Bucket']
    s3_args = "-s3endpointUrl=" + s3endpoint + " -s3accessKey=" + s3accesskey + " -s3secretKey=" + s3secretkey + " -s3objectStoreLocation=" + s3objectstorelocation 
    if 'app_args' in args:
      app_args = s3_args + " " + args['app_args']
    else:
      app_args = s3_args
    job_cpu = args['job_cpu']
    job_memory = args['job_memory']
    job_image = "docker-registry.default.svc:5000/" + namespace + "/" + name

    # Header for HTTP Request
    auth = "Bearer " + token
    headers = {"Authorization": auth, "Accept": "application/json",
               "Connection": "close", "Content-Type": "application/json"}
    output_name = name + ":" + "latest"
    empty_payload = json.dumps({})

    # Payload for Image Stream
    payload_image = {
                     "kind": "ImageStream",
                     "metadata": {
                       "labels": {
                          "app": name
                          },
                       "name": name
                      }
                    }

    # Check if the Image Stream exists already. If so, delete it
    image_url = url_imagestream + "/" + name
    response = requests.get(image_url, headers=headers, verify=False)
    r = response.json()
    if (r['kind'] == "ImageStream"):
      del_response = requests.delete(image_url, headers=headers, verify=False)

    # HTTP request for Image Stream
    data_imagestream = json.dumps(payload_image)
    response = requests.post(url_imagestream, data=data_imagestream,
                             headers=headers, verify=False)

    # Payload for BuildConfig
    payload_buildconfig = {
                           "apiVersion": "build.openshift.io/v1",
                           "kind": "BuildConfig",
                           "metadata": {
                             "labels": {
                                "app": name
                               },
                             "name": name
                            },
                           "spec": {
                             "resources": {
                               "requests": {
                                 "cpu": buildconfig_cpu,
                                 "memory": buildconfig_memory
                                }
                              },
                             "output": {
                               "to": {
                                 "kind": "ImageStreamTag",
                                 "name": output_name
                                }
                              },
                             "runPolicy": "Serial",
                             "source": {
                               "git": {
                                 "uri": buildconfig_git_url
                                },
                               "type": "Git"
                              },
                             "strategy": {
                               "sourceStrategy": {
                                 "env": [{
                                   "name": "APP_FILE",
                                   "value": app_file
                                  }, {
                                   "name": "GIT_SSL_NO_VERIFY",
                                   "value": "true"
                                  }],
                                 "forcePull": True,
                                 "from": {
                                   "kind": "DockerImage",
                                   "name": buildconfig_image
                                  }
                                },
                               "type": "Source"
                              },
                             "triggers": [{
                               "type": "ConfigChange"
                               }, {
                               "github": {
                                  "secret": name
                                 },
                               "type": "GitHub"
                               }, {
                               "generic": {
                                 "secret": name
                                },
                               "type": "Generic"
                               }]
                           },
                           "status": {
                             "lastVersion": "0"
                           }
                          }

    # Check if the BuildConfig exists already. If so, delete it
    build_url = url_buildconfig + "/" + name
    response = requests.get(build_url, headers=headers, verify=False)
    r = response.json()
    if (r['kind'] == "BuildConfig"):
      del_response = requests.delete(build_url, headers=headers, verify=False)

    # HTTP request for BuildConfig
    data_buildconfig = json.dumps(payload_buildconfig)
    response = requests.post(url_buildconfig, data=data_buildconfig,
                             headers=headers, verify=False)

    # Payload for Job
    payload_job = {
                   "apiVersion": "batch/v1",
                   "kind": "Job",
                   "metadata": {
                     "labels": {
                       "app": name
                      },
                     "name": name
                     },
                   "spec": {
                     "completions": "1",
                     "parallelism": "1",
                     "template": {
                       "metadata": {
                         "labels": {
                           "job-name": name
                          },
                         "name": name
                         },
                       "spec": {
                         "containers": [{
                           "env": [{
                             "name": "OSHINKO_CLUSTER_NAME",
                             "value": "spark-cluster"
                              }, {
                             "name": "APP_ARGS",
                             "value": app_args
                              }, {
                             "name": "APP_EXIT",
                             "value": "true"
                              }, {
                             "name": "POD_NAME",
                             "valueFrom": {
                              "fieldRef": {
                                "apiVersion": "v1",
                                "fieldPath": "metadata.name"
                               }
                             }
                           }],
                         "image": job_image,
                         "imagePullPolicy": "Always",
                         "name": name,
                         "resources": {
                           "requests": {
                            "cpu": job_cpu,
                            "memory": job_memory
                            },
                          }
                         }],
                        "restartPolicy": "OnFailure",
                        "schedulerName": "default-scheduler",
                        "serviceAccount": "oshinko",
                        "serviceAccountName": "oshinko"
                       }
                     }
                   }
                 }

    # Check if the job exists already. If so, delete it
    joburl = url_job + "/" + name
    response = requests.get(joburl, headers=headers, verify=False)
    r = response.json()
    if (r['kind'] == "Job"):
      del_response = requests.delete(joburl, headers=headers, verify=False)

    # Delete any existing pods created by previous job
    url_pods = server + "/api/v1/namespaces/" + namespace + "/pods"
    response = requests.get(url_pods, headers=headers, verify=False)
    r = response.json()
    if 'items' in r:
     for item in r['items']:
      if 'metadata' in item:
       if 'labels' in item['metadata']:
         if 'job-name' in item['metadata']['labels']:
           job = item['metadata']['labels']['job-name']
           if job == name:
             urlpods = url_pods + "/" + item['metadata']['name']
             response = requests.delete(urlpods, headers=headers, verify=False)

    # Create new job
    data_job = json.dumps(payload_job)
    response = requests.post(url_job, data=data_job, headers=headers,
                             verify=False)
    return {"response": response.text}

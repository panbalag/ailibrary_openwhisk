# Storage

This library contains s3 enabled methods to access object storage.

# Methods

`create_session_and_resource` - create session to access object storage

`get_objects` - get contents in a bucket.

`upload_file` - upload file to a given destination.

`upload_folder` - upload folder and its contents to a given destination.

`download_file` - download file from a given location.

`download_folder` - download folder and its contents from a given location.

# AI-Library

AI-Library is an open source collection of AI components including machine learning algorithms as well as machine learning solutions to common use cases from product and services teams. AI-Library enables users to implement machine learning models and solutions as stand-alone services or integrate into existing workflows to perform common and complex functions. AI-Library is a part of <a href="https://gitlab.com/opendatahub">Open Data Hub</a>, a collection of open source and cloud components deployed as a “machine learning-as-a-service” platform, built on top of OpenShift.


# Contents

## Models

`associative_rule_learning`  - rule based machine learning method to discover interesting relations between variables

`correlation_analysis` - statistical method to measure the strength of relationship between two variables.

`duplicate_bug_detection` -  topic modeling based method to report top matches of existing bugs in the system to newly reported bugs.

`flake_analysis` - clustering based detection of false positives in test failures.

`matrix_factorization` - hierarchical poisson factorization model that provides companion dependencies for application stacks.

`sentiment_analysis` - nlp model to identify and categorize opinions in texts.

`linear_regression` - model to establish relationship between dependent variable and one or more independent variables using best fit stright line.

## Supporting Libraries

`actions` - REST APIs to trigger OpenWhisk actions that inturn execute Machine Learning models as jobs well as poll the status on such jobs.

`ansible` - Ansible automation to deploy the above OpenWhisk actions.

`storage` - s3 library routines to access object storage (read or write data)

`accuracy_measures` - library to calculate predictive accuracy measures such as mean absolute error and several others.

# Workflow

`Save Data` -----> `Run Model` -----> `Use Results`

## Save Data

The models implemented in this library work on data stored in the object storage. So the first step in using the implemented models is to save the data in to the object storage. The object storage used for this purpose needs to be s3 compatible since the storage routines use boto3/s3 api. Any client or cli interface that is s3 compatible could be used to upload data in to the storage (a few utilities have been listed below).

* <a href="http://docs.ceph.com/docs/giant/man/8/rados/">RADOS Object Storage Utility</a>
* <a href="http://docs.ceph.com/docs/dumpling/radosgw/s3/">RADOS S3 API</a>
* <a href="https://docs.aws.amazon.com/cli/latest/reference/s3/index.html#cli-aws-s3">AWS S3 CLI</a>
* <a href="http://www.crossftp.com/">CrossFTP (Amazon S3 Client)</a>

## Run Model

The models can be downloaded and executed as standalone python machine learning modules. But for the purpose of integrating into existing infrastructure like CI, we provide supporting libraries that can be used to run models on container application platforms like OpenShift. The supporting libraries enable users to deploy OpenWhisk actions which can run the machine learning models as jobs on the container application platform. Through the REST APIs provided by OpenWhisk, users can submit requests to execute a specific machine learning model as well as poll the status of a submitted request. Sample requests for training a model or running prediction and poll for the status on a submitted request are shown below. For information specific to each model, please refer to the README section for the corresponding model.

#### Train Model/Run Prediction


    curl -u [Basic-Auth] [OpenWhisk endpoint for the specific model]
    -X POST -H "Content-Type: application/json" -d 
    '{ 
       "name"       : "Identifier for the job", 
       "app_args"   : "Model specific arguments"
     }'
     
#### Poll Status

    curl -u [Basic-Auth] [OpenWhisk endpoint to poll status]
    -X POST -H "Content-Type: application/json" -d 
    '{ 
       "name"       : "Identifier for the job"
     }'

## Use Results

Results are specific to each machine learning model and are stored in the object storage at the location pointed by the user (this is provided through the app_args in the REST API). 


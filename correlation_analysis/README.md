# Correlation Analysis

Correlation analysis is a statistical method used to measure the strength of relationship between two variables.

# Content

`correlation.py` - Perform univariate analysis as well as multivariate study of given features in the dataset.
#### Parameters
* s3Path - path to file that contains metadata information on input data.
* parser - location where input data is stored (in parquet format)
* columns - variables to perform analysis on
* date - top level object key where metadata information and input data is stored
* destination - location (including filename) to store results

# Workflow

## Save Data

Sample data used in this analysis can be found in spreadsheet format under 'data' directory. The data lists bugs from openstack-neutron team. This data was used to calculate bugs_reported_per_week, bugs_fixed_per_week, bugs_verified_per_week and bug_fix_released_per_week for the openstack-neutron team. Note that the data containing the four columns were stored in parquet format.

The following example shows file that contains metadata information about the input data.

    s3Path - openstack_neutron/parsers.json
    {
        "parsers": [
            {
                "name": "bug_fix_verify_release_data",
                "columns": [
                    {
                        "name": "week",
                        "type": "Categorical"
                    },
                    {
                        "name": "bugs_reported_per_week",
                        "type": "Numeric"
                    },
                    {
                        "name": "bugs_fixed_per_week",
                        "type": "Numeric"
                    },
                    {
                        "name": "bugs_verified_per_week",
                        "type": "Numeric"
                    },
                    {
                        "name": "bug_fix_released_per_week",
                        "type": "Numeric"
                    }
                ]
            },
            {
                "name": "sample_data",
                "columns": [
                    {
                        "name": "feature1",
                        "type": "Categorical"
                    },
                    {
                        "name": "feature2",
                        "type": "Numeric"
                    },
                    {
                        "name": "feature3",
                        "type": "Numeric"
                    },
                    {
                        "name": "feature4",
                        "type": "Numeric"
                    },
                    {
                        "name": "feature5",
                        "type": "Categorical"
                    }
                ]
            }
        ]
    }

The following example shows the input data in parquet format

    parser - bug_fix_verify_release_data
    File -
        0_0_0.parquet

## Run Model

#### Correlation Analysis

The following example shows correlation analysis between bugs_reported_per_week and bugs_fixed_per_week.

    curl -u user:secret \
    "https://openwhisk-dh-prod-ow.openshift.com/api/v1/namespaces/_/actions/ai-library/correlation-analysis?" \
    -X POST -H "Content-Type: application/json" -d \
    '{"name":"correlation-analysis-dev", 
      "app_args": "-s3Path=openstack_neutron/parsers.json -parser=bug_fix_verify_release_data -columns=bugs_reported_per_week,bugs_fixed_per_week -date=openstack_neutron -destination=openstack_neutron/result.json"}'

The following example shows correlation analysis between bugs_verified_per_week and bug_fix_released_per_week.

    curl -u 789c46b1-71f6-4ed5-8c54-816aa4f8c502:HRINUbNQR6m8bd31jILJ0p7h0pXBl0885vTRh5hcKRdWQQ6rqeVu1jdsS31YaqHb \
    "https://openwhisk-dh-prod-ow.cloud.datahub.upshift.redhat.com/api/v1/namespaces/_/actions/ai-library/correlation-analysis?blocking=true&result=true" \
    -X POST -H "Content-Type: application/json" -d \
    '{"name":"correlation-analysis3", 
      "app_args": "-s3Path=openstack_neutron/parsers.json -parser=bug_fix_verify_release_data -columns=bugs_verified_per_week,bug_fix_released_per_week -date=openstack_neutron -destination=openstack_neutron/result.json"}'
      
#### Poll Status

    curl -u user:secret \
    "https://openwhisk-dh-prod-ow.openshift.com/api/v1/namespaces/_/actions/ai-library/poll-status?" \
    -X POST -H "Content-Type: application/json" -d \
    '{ "name"               : "correlation-analysis-dev"}'

# Use Results

Results are stored as json serialized list of graphs that contains the following details
* Summary statistics of each column
* Pearson correlation coefficient showing the strength of relationship between the variables
* Box plots and Scatter plots between the variables

### [bugs_reported_per_week] vs [bugs_fixed_per_week]

        {
          "metadata": {
            "type": "plotly_graphs",
            "bucket": "DH-DEV-DATA",
            "asyncJob": {
              "status": "complete"
            }
          },
          "data": {
            "date": "openstack_neutron",
            "parsers": [
              {
                "name": "bug_fix_verify_release_data",
                "columns": [
                  "bugs_reported_per_week",
                  "bugs_fixed_per_week"
                ]
              }
            ],
            "graphs": [
              {
                "label": "Description N",
                "plot": {
                  "data": [
                    {
                      "cells": {
                        "align": [
                          "left",
                          "center"
                        ],
                        "fill": {
                          "color": [
                            "#25FEFD",
                            "white"
                          ]
                        },
                        "font": {
                          "color": "#506784",
                          "size": 12
                        },
                        "height": 30,
                        "line": {
                          "color": "#506784"
                        },
                        "values": [
                          [
                            "Total count",
                            "Average value",
                            "Standard deviation",
                            "Minimum value",
                            "First Quartile (25%)",
                            "Median (50%)",
                            "Third Quartile (75%)",
                            "Maximum value"
                          ],
                          [
                            62,
                            5.870967741935484,
                            4.119063521484504,
                            0,
                            2.25,
                            5,
                            8,
                            17
                          ],
                          [
                            62,
                            5.870967741935484,
                            6.320624245389167,
                            0,
                            1,
                            4,
                            8.75,
                            28
                          ]
                        ]
                      },
                      "header": {
                        "align": [
                          "left",
                          "center"
                        ],
                        "fill": {
                          "color": "#119DFF"
                        },
                        "font": {
                          "color": "white",
                          "size": 12
                        },
                        "height": 40,
                        "line": {
                          "color": "#506784"
                        },
                        "values": [
                          [
                            "<b>Basic statistic comparison</b>"
                          ],
                          [
                            "<b> bugs_reported_per_week </b>"
                          ],
                          [
                            "<b> bugs_fixed_per_week </b>"
                          ]
                        ]
                      },
                      "type": "table",
                      "uid": "4d5f20ee-e15a-11e8-9672-0a58ac2844c6"
                    }
                  ],
                  "layout": {
                    "title": "Summary Table for ['bugs_reported_per_week', 'bugs_fixed_per_week']"
                  }
                }
              },
              {
                "label": "Correlation",
                "plot": {
                  "data": [
                    {
                      "cells": {
                        "align": [
                          "left",
                          "center"
                        ],
                        "fill": {
                          "color": [
                            "#25FEFD",
                            "white"
                          ]
                        },
                        "font": {
                          "color": "#506784",
                          "size": 12
                        },
                        "height": 30,
                        "line": {
                          "color": "#506784"
                        },
                        "values": [
                          [
                            "1. The correlation coefficient is",
                            "2. P value is",
                            "3. Conclusion"
                          ],
                          [
                            0.16621207075152225,
                            0.1966610656577842,
                            "Two datasets have positive small correlation and this result is statistically insignificant."
                          ]
                        ]
                      },
                      "header": {
                        "align": [
                          "left",
                          "center"
                        ],
                        "fill": {
                          "color": "#119DFF"
                        },
                        "font": {
                          "color": "white",
                          "size": 12
                        },
                        "height": 40,
                        "line": {
                          "color": "#506784"
                        },
                        "values": [
                          [
                            "<b>Simple Analysis on Correlation</b>"
                          ],
                          [
                            "<b>Result</b>"
                          ]
                        ]
                      },
                      "type": "table",
                      "uid": "4d63d8f0-e15a-11e8-9672-0a58ac2844c6"
                    }
                  ],
                  "layout": {
                    "title": "Correlation Table for bugs_reported_per_week, bugs_fixed_per_week"
                  }
                }
              },
              {
                "label": "Comparison",
                "plot": {
                  "data": [
                    {
                      "boxmean": true,
                      "marker": {
                        "color": "rgb(8, 81, 156)"
                      },
                      "name": "bugs_reported_per_week",
                      "y": [
                        1,
                        0,
                        2,
                        4,
                        7,
                        7,
                        11,
                        8,
                        4,
                        2,
                        8,
                        9,
                        8,
                        2,
                        6,
                        6,
                        4,
                        6,
                        4,
                        4,
                        0,
                        2,
                        5,
                        2,
                        1,
                        1,
                        9,
                        3,
                        8,
                        3,
                        4,
                        5,
                        6,
                        4,
                        7,
                        1,
                        6,
                        5,
                        7,
                        11,
                        4,
                        2,
                        1,
                        10,
                        13,
                        8,
                        8,
                        11,
                        17,
                        17,
                        12,
                        11,
                        9,
                        13,
                        5,
                        5,
                        10,
                        10,
                        5,
                        0,
                        0,
                        0
                      ],
                      "type": "box",
                      "uid": "4d73a1c2-e15a-11e8-9672-0a58ac2844c6",
                      "xaxis": "x",
                      "yaxis": "y"
                    },
                    {
                      "boxmean": true,
                      "marker": {
                        "color": "rgb(8, 81, 156)"
                      },
                      "name": "bugs_fixed_per_week",
                      "y": [
                        0,
                        0,
                        0,
                        0,
                        0,
                        15,
                        4,
                        3,
                        4,
                        0,
                        12,
                        0,
                        23,
                        2,
                        0,
                        12,
                        5,
                        0,
                        7,
                        4,
                        2,
                        6,
                        0,
                        0,
                        8,
                        4,
                        3,
                        2,
                        0,
                        4,
                        7,
                        7,
                        8,
                        2,
                        4,
                        5,
                        4,
                        9,
                        2,
                        0,
                        12,
                        3,
                        3,
                        2,
                        1,
                        9,
                        4,
                        18,
                        0,
                        19,
                        11,
                        9,
                        28,
                        0,
                        15,
                        20,
                        1,
                        5,
                        11,
                        5,
                        12,
                        8
                      ],
                      "type": "box",
                      "uid": "4d73ed58-e15a-11e8-9672-0a58ac2844c6",
                      "xaxis": "x2",
                      "yaxis": "y2"
                    }
                  ],
                  "layout": {
                    "xaxis": {
                      "anchor": "y",
                      "domain": [
                        0,
                        0.45
                      ]
                    },
                    "yaxis": {
                      "anchor": "x",
                      "domain": [
                        0,
                        1
                      ]
                    },
                    "xaxis2": {
                      "anchor": "y2",
                      "domain": [
                        0.55,
                        1
                      ]
                    },
                    "yaxis2": {
                      "anchor": "x2",
                      "domain": [
                        0,
                        1
                      ]
                    },
                    "title": "Box plot comparison between bugs_reported_per_week and bugs_fixed_per_week"
                  }
                }
              },
              {
                "label": "Skewness",
                "plot": {
                  "data": [
                    {
                      "marker": {
                        "color": "rgb(0, 0, 100)"
                      },
                      "name": "bugs_reported_per_week",
                      "x": [
                        1,
                        0,
                        2,
                        4,
                        7,
                        7,
                        11,
                        8,
                        4,
                        2,
                        8,
                        9,
                        8,
                        2,
                        6,
                        6,
                        4,
                        6,
                        4,
                        4,
                        0,
                        2,
                        5,
                        2,
                        1,
                        1,
                        9,
                        3,
                        8,
                        3,
                        4,
                        5,
                        6,
                        4,
                        7,
                        1,
                        6,
                        5,
                        7,
                        11,
                        4,
                        2,
                        1,
                        10,
                        13,
                        8,
                        8,
                        11,
                        17,
                        17,
                        12,
                        11,
                        9,
                        13,
                        5,
                        5,
                        10,
                        10,
                        5,
                        0,
                        0,
                        0
                      ],
                      "type": "histogram",
                      "uid": "4d8bfe2a-e15a-11e8-9672-0a58ac2844c6",
                      "xaxis": "x",
                      "yaxis": "y"
                    },
                    {
                      "marker": {
                        "color": "rgb(8, 81, 156)"
                      },
                      "name": "bugs_fixed_per_week",
                      "x": [
                        0,
                        0,
                        0,
                        0,
                        0,
                        15,
                        4,
                        3,
                        4,
                        0,
                        12,
                        0,
                        23,
                        2,
                        0,
                        12,
                        5,
                        0,
                        7,
                        4,
                        2,
                        6,
                        0,
                        0,
                        8,
                        4,
                        3,
                        2,
                        0,
                        4,
                        7,
                        7,
                        8,
                        2,
                        4,
                        5,
                        4,
                        9,
                        2,
                        0,
                        12,
                        3,
                        3,
                        2,
                        1,
                        9,
                        4,
                        18,
                        0,
                        19,
                        11,
                        9,
                        28,
                        0,
                        15,
                        20,
                        1,
                        5,
                        11,
                        5,
                        12,
                        8
                      ],
                      "type": "histogram",
                      "uid": "4d8c703a-e15a-11e8-9672-0a58ac2844c6",
                      "xaxis": "x2",
                      "yaxis": "y2"
                    }
                  ],
                  "layout": {
                    "xaxis": {
                      "anchor": "y",
                      "domain": [
                        0,
                        0.45
                      ]
                    },
                    "yaxis": {
                      "anchor": "x",
                      "domain": [
                        0,
                        1
                      ]
                    },
                    "xaxis2": {
                      "anchor": "y2",
                      "domain": [
                        0.55,
                        1
                      ]
                    },
                    "yaxis2": {
                      "anchor": "x2",
                      "domain": [
                        0,
                        1
                      ]
                    },
                    "title": "Skewness comparison between bugs_reported_per_week and bugs_fixed_per_week"
                  }
                }
              },
              {
                "label": "Skew Conclusion",
                "plot": {
                  "data": [
                    {
                      "cells": {
                        "align": [
                          "left",
                          "center"
                        ],
                        "fill": {
                          "color": [
                            "#25FEFD",
                            "white"
                          ]
                        },
                        "font": {
                          "color": "#506784",
                          "size": 12
                        },
                        "height": 30,
                        "line": {
                          "color": "#506784"
                        },
                        "values": [
                          [
                            "<b> bugs_reported_per_week: </b>The data has normal distribution.",
                            "<b> bugs_fixed_per_week: </b>The data has right-skewed distribution, there is a long tail in the positive direction on the number line. The mean is also to the right of the peak."
                          ]
                        ]
                      },
                      "header": {
                        "align": [
                          "left",
                          "center"
                        ],
                        "fill": {
                          "color": "#119DFF"
                        },
                        "font": {
                          "color": "white",
                          "size": 12
                        },
                        "height": 40,
                        "line": {
                          "color": "#506784"
                        },
                        "values": [
                          [
                            "<b>Conclustion</b>"
                          ]
                        ]
                      },
                      "type": "table",
                      "uid": "4d8f1b00-e15a-11e8-9672-0a58ac2844c6"
                    }
                  ],
                  "layout": {
                    "title": "Skewness conclusion for bugs_reported_per_week, bugs_fixed_per_week"
                  }
                }
              },
              {
                "label": "Scatter",
                "plot": {
                  "data": [
                    {
                      "marker": {
                        "color": "#119DFF",
                        "line": {
                          "width": 1
                        }
                      },
                      "mode": "markers",
                      "x": [
                        1,
                        0,
                        2,
                        4,
                        7,
                        7,
                        11,
                        8,
                        4,
                        2,
                        8,
                        9,
                        8,
                        2,
                        6,
                        6,
                        4,
                        6,
                        4,
                        4,
                        0,
                        2,
                        5,
                        2,
                        1,
                        1,
                        9,
                        3,
                        8,
                        3,
                        4,
                        5,
                        6,
                        4,
                        7,
                        1,
                        6,
                        5,
                        7,
                        11,
                        4,
                        2,
                        1,
                        10,
                        13,
                        8,
                        8,
                        11,
                        17,
                        17,
                        12,
                        11,
                        9,
                        13,
                        5,
                        5,
                        10,
                        10,
                        5,
                        0,
                        0,
                        0
                      ],
                      "y": [
                        0,
                        0,
                        0,
                        0,
                        0,
                        15,
                        4,
                        3,
                        4,
                        0,
                        12,
                        0,
                        23,
                        2,
                        0,
                        12,
                        5,
                        0,
                        7,
                        4,
                        2,
                        6,
                        0,
                        0,
                        8,
                        4,
                        3,
                        2,
                        0,
                        4,
                        7,
                        7,
                        8,
                        2,
                        4,
                        5,
                        4,
                        9,
                        2,
                        0,
                        12,
                        3,
                        3,
                        2,
                        1,
                        9,
                        4,
                        18,
                        0,
                        19,
                        11,
                        9,
                        28,
                        0,
                        15,
                        20,
                        1,
                        5,
                        11,
                        5,
                        12,
                        8
                      ],
                      "type": "scatter",
                      "uid": "4da7d88e-e15a-11e8-9672-0a58ac2844c6"
                    }
                  ],
                  "layout": {
                    "hovermode": "closest",
                    "title": "Scatter plot of the data across bugs_reported_per_week and bugs_fixed_per_week",
                    "xaxis": {
                      "ticklen": 5,
                      "title": "bugs_reported_per_week"
                    },
                    "yaxis": {
                      "ticklen": 5,
                      "title": "bugs_fixed_per_week"
                    }
                  }
                }
              }
            ]
          },
          "errors": []
        }
        
        
### [bugs_verified_per_week] vs [bug_fix_released_per_week]

        {
          "metadata": {
            "type": "plotly_graphs",
            "bucket": "DH-DEV-DATA",
            "asyncJob": {
              "status": "complete"
            }
          },
          "data": {
            "date": "openstack_neutron",
            "parsers": [
              {
                "name": "bug_fix_verify_release_data",
                "columns": [
                  "bugs_verified_per_week",
                  "bug_fix_released_per_week"
                ]
              }
            ],
            "graphs": [
              {
                "label": "Description N",
                "plot": {
                  "data": [
                    {
                      "cells": {
                        "align": [
                          "left",
                          "center"
                        ],
                        "fill": {
                          "color": [
                            "#25FEFD",
                            "white"
                          ]
                        },
                        "font": {
                          "color": "#506784",
                          "size": 12
                        },
                        "height": 30,
                        "line": {
                          "color": "#506784"
                        },
                        "values": [
                          [
                            "Total count",
                            "Average value",
                            "Standard deviation",
                            "Minimum value",
                            "First Quartile (25%)",
                            "Median (50%)",
                            "Third Quartile (75%)",
                            "Maximum value"
                          ],
                          [
                            62,
                            5.870967741935484,
                            5.068332641947859,
                            0,
                            3,
                            4,
                            7.75,
                            25
                          ],
                          [
                            62,
                            5.870967741935484,
                            5.995856139985462,
                            0,
                            0.25,
                            4,
                            9,
                            26
                          ]
                        ]
                      },
                      "header": {
                        "align": [
                          "left",
                          "center"
                        ],
                        "fill": {
                          "color": "#119DFF"
                        },
                        "font": {
                          "color": "white",
                          "size": 12
                        },
                        "height": 40,
                        "line": {
                          "color": "#506784"
                        },
                        "values": [
                          [
                            "<b>Basic statistic comparison</b>"
                          ],
                          [
                            "<b> bugs_verified_per_week </b>"
                          ],
                          [
                            "<b> bug_fix_released_per_week </b>"
                          ]
                        ]
                      },
                      "type": "table",
                      "uid": "ca514374-e15b-11e8-8120-0a58ac284a16"
                    }
                  ],
                  "layout": {
                    "title": "Summary Table for ['bugs_verified_per_week', 'bug_fix_released_per_week']"
                  }
                }
              },
              {
                "label": "Correlation",
                "plot": {
                  "data": [
                    {
                      "cells": {
                        "align": [
                          "left",
                          "center"
                        ],
                        "fill": {
                          "color": [
                            "#25FEFD",
                            "white"
                          ]
                        },
                        "font": {
                          "color": "#506784",
                          "size": 12
                        },
                        "height": 30,
                        "line": {
                          "color": "#506784"
                        },
                        "values": [
                          [
                            "1. The correlation coefficient is",
                            "2. P value is",
                            "3. Conclusion"
                          ],
                          [
                            0.5637112917826917,
                            0.000001836388331816622,
                            "Two datasets have positive large/strong correlation and this result is statistically significant."
                          ]
                        ]
                      },
                      "header": {
                        "align": [
                          "left",
                          "center"
                        ],
                        "fill": {
                          "color": "#119DFF"
                        },
                        "font": {
                          "color": "white",
                          "size": 12
                        },
                        "height": 40,
                        "line": {
                          "color": "#506784"
                        },
                        "values": [
                          [
                            "<b>Simple Analysis on Correlation</b>"
                          ],
                          [
                            "<b>Result</b>"
                          ]
                        ]
                      },
                      "type": "table",
                      "uid": "ca563b7c-e15b-11e8-8120-0a58ac284a16"
                    }
                  ],
                  "layout": {
                    "title": "Correlation Table for bugs_verified_per_week, bug_fix_released_per_week"
                  }
                }
              },
              {
                "label": "Comparison",
                "plot": {
                  "data": [
                    {
                      "boxmean": true,
                      "marker": {
                        "color": "rgb(8, 81, 156)"
                      },
                      "name": "bugs_verified_per_week",
                      "y": [
                        0,
                        0,
                        0,
                        2,
                        5,
                        9,
                        3,
                        3,
                        4,
                        6,
                        16,
                        3,
                        10,
                        2,
                        3,
                        9,
                        5,
                        4,
                        3,
                        4,
                        4,
                        4,
                        3,
                        4,
                        1,
                        4,
                        3,
                        2,
                        4,
                        1,
                        11,
                        4,
                        6,
                        6,
                        1,
                        8,
                        5,
                        4,
                        3,
                        3,
                        11,
                        2,
                        3,
                        2,
                        2,
                        9,
                        7,
                        14,
                        8,
                        18,
                        13,
                        25,
                        4,
                        5,
                        21,
                        7,
                        6,
                        6,
                        7,
                        14,
                        8,
                        0
                      ],
                      "type": "box",
                      "uid": "ca6248fe-e15b-11e8-8120-0a58ac284a16",
                      "xaxis": "x",
                      "yaxis": "y"
                    },
                    {
                      "boxmean": true,
                      "marker": {
                        "color": "rgb(8, 81, 156)"
                      },
                      "name": "bug_fix_released_per_week",
                      "y": [
                        0,
                        0,
                        0,
                        0,
                        0,
                        15,
                        4,
                        3,
                        4,
                        0,
                        12,
                        12,
                        11,
                        2,
                        0,
                        12,
                        5,
                        0,
                        7,
                        4,
                        2,
                        6,
                        0,
                        0,
                        8,
                        4,
                        3,
                        2,
                        0,
                        4,
                        9,
                        5,
                        8,
                        2,
                        4,
                        5,
                        8,
                        5,
                        2,
                        0,
                        12,
                        3,
                        5,
                        0,
                        1,
                        9,
                        4,
                        18,
                        0,
                        21,
                        9,
                        12,
                        26,
                        0,
                        15,
                        20,
                        0,
                        5,
                        12,
                        4,
                        12,
                        8
                      ],
                      "type": "box",
                      "uid": "ca62978c-e15b-11e8-8120-0a58ac284a16",
                      "xaxis": "x2",
                      "yaxis": "y2"
                    }
                  ],
                  "layout": {
                    "xaxis": {
                      "anchor": "y",
                      "domain": [
                        0,
                        0.45
                      ]
                    },
                    "yaxis": {
                      "anchor": "x",
                      "domain": [
                        0,
                        1
                      ]
                    },
                    "xaxis2": {
                      "anchor": "y2",
                      "domain": [
                        0.55,
                        1
                      ]
                    },
                    "yaxis2": {
                      "anchor": "x2",
                      "domain": [
                        0,
                        1
                      ]
                    },
                    "title": "Box plot comparison between bugs_verified_per_week and bug_fix_released_per_week"
                  }
                }
              },
              {
                "label": "Skewness",
                "plot": {
                  "data": [
                    {
                      "marker": {
                        "color": "rgb(0, 0, 100)"
                      },
                      "name": "bugs_verified_per_week",
                      "x": [
                        0,
                        0,
                        0,
                        2,
                        5,
                        9,
                        3,
                        3,
                        4,
                        6,
                        16,
                        3,
                        10,
                        2,
                        3,
                        9,
                        5,
                        4,
                        3,
                        4,
                        4,
                        4,
                        3,
                        4,
                        1,
                        4,
                        3,
                        2,
                        4,
                        1,
                        11,
                        4,
                        6,
                        6,
                        1,
                        8,
                        5,
                        4,
                        3,
                        3,
                        11,
                        2,
                        3,
                        2,
                        2,
                        9,
                        7,
                        14,
                        8,
                        18,
                        13,
                        25,
                        4,
                        5,
                        21,
                        7,
                        6,
                        6,
                        7,
                        14,
                        8,
                        0
                      ],
                      "type": "histogram",
                      "uid": "ca71fed4-e15b-11e8-8120-0a58ac284a16",
                      "xaxis": "x",
                      "yaxis": "y"
                    },
                    {
                      "marker": {
                        "color": "rgb(8, 81, 156)"
                      },
                      "name": "bug_fix_released_per_week",
                      "x": [
                        0,
                        0,
                        0,
                        0,
                        0,
                        15,
                        4,
                        3,
                        4,
                        0,
                        12,
                        12,
                        11,
                        2,
                        0,
                        12,
                        5,
                        0,
                        7,
                        4,
                        2,
                        6,
                        0,
                        0,
                        8,
                        4,
                        3,
                        2,
                        0,
                        4,
                        9,
                        5,
                        8,
                        2,
                        4,
                        5,
                        8,
                        5,
                        2,
                        0,
                        12,
                        3,
                        5,
                        0,
                        1,
                        9,
                        4,
                        18,
                        0,
                        21,
                        9,
                        12,
                        26,
                        0,
                        15,
                        20,
                        0,
                        5,
                        12,
                        4,
                        12,
                        8
                      ],
                      "type": "histogram",
                      "uid": "ca727602-e15b-11e8-8120-0a58ac284a16",
                      "xaxis": "x2",
                      "yaxis": "y2"
                    }
                  ],
                  "layout": {
                    "xaxis": {
                      "anchor": "y",
                      "domain": [
                        0,
                        0.45
                      ]
                    },
                    "yaxis": {
                      "anchor": "x",
                      "domain": [
                        0,
                        1
                      ]
                    },
                    "xaxis2": {
                      "anchor": "y2",
                      "domain": [
                        0.55,
                        1
                      ]
                    },
                    "yaxis2": {
                      "anchor": "x2",
                      "domain": [
                        0,
                        1
                      ]
                    },
                    "title": "Skewness comparison between bugs_verified_per_week and bug_fix_released_per_week"
                  }
                }
              },
              {
                "label": "Skew Conclusion",
                "plot": {
                  "data": [
                    {
                      "cells": {
                        "align": [
                          "left",
                          "center"
                        ],
                        "fill": {
                          "color": [
                            "#25FEFD",
                            "white"
                          ]
                        },
                        "font": {
                          "color": "#506784",
                          "size": 12
                        },
                        "height": 30,
                        "line": {
                          "color": "#506784"
                        },
                        "values": [
                          [
                            "<b> bugs_verified_per_week: </b>The data has right-skewed distribution, there is a long tail in the positive direction on the number line. The mean is also to the right of the peak.",
                            "<b> bug_fix_released_per_week: </b>The data has right-skewed distribution, there is a long tail in the positive direction on the number line. The mean is also to the right of the peak."
                          ]
                        ]
                      },
                      "header": {
                        "align": [
                          "left",
                          "center"
                        ],
                        "fill": {
                          "color": "#119DFF"
                        },
                        "font": {
                          "color": "white",
                          "size": 12
                        },
                        "height": 40,
                        "line": {
                          "color": "#506784"
                        },
                        "values": [
                          [
                            "<b>Conclustion</b>"
                          ]
                        ]
                      },
                      "type": "table",
                      "uid": "ca769958-e15b-11e8-8120-0a58ac284a16"
                    }
                  ],
                  "layout": {
                    "title": "Skewness conclusion for bugs_verified_per_week, bug_fix_released_per_week"
                  }
                }
              },
              {
                "label": "Scatter",
                "plot": {
                  "data": [
                    {
                      "marker": {
                        "color": "#119DFF",
                        "line": {
                          "width": 1
                        }
                      },
                      "mode": "markers",
                      "x": [
                        0,
                        0,
                        0,
                        2,
                        5,
                        9,
                        3,
                        3,
                        4,
                        6,
                        16,
                        3,
                        10,
                        2,
                        3,
                        9,
                        5,
                        4,
                        3,
                        4,
                        4,
                        4,
                        3,
                        4,
                        1,
                        4,
                        3,
                        2,
                        4,
                        1,
                        11,
                        4,
                        6,
                        6,
                        1,
                        8,
                        5,
                        4,
                        3,
                        3,
                        11,
                        2,
                        3,
                        2,
                        2,
                        9,
                        7,
                        14,
                        8,
                        18,
                        13,
                        25,
                        4,
                        5,
                        21,
                        7,
                        6,
                        6,
                        7,
                        14,
                        8,
                        0
                      ],
                      "y": [
                        0,
                        0,
                        0,
                        0,
                        0,
                        15,
                        4,
                        3,
                        4,
                        0,
                        12,
                        12,
                        11,
                        2,
                        0,
                        12,
                        5,
                        0,
                        7,
                        4,
                        2,
                        6,
                        0,
                        0,
                        8,
                        4,
                        3,
                        2,
                        0,
                        4,
                        9,
                        5,
                        8,
                        2,
                        4,
                        5,
                        8,
                        5,
                        2,
                        0,
                        12,
                        3,
                        5,
                        0,
                        1,
                        9,
                        4,
                        18,
                        0,
                        21,
                        9,
                        12,
                        26,
                        0,
                        15,
                        20,
                        0,
                        5,
                        12,
                        4,
                        12,
                        8
                      ],
                      "type": "scatter",
                      "uid": "ca85fcfe-e15b-11e8-8120-0a58ac284a16"
                    }
                  ],
                  "layout": {
                    "hovermode": "closest",
                    "title": "Scatter plot of the data across bugs_verified_per_week and bug_fix_released_per_week",
                    "xaxis": {
                      "ticklen": 5,
                      "title": "bugs_verified_per_week"
                    },
                    "yaxis": {
                      "ticklen": 5,
                      "title": "bug_fix_released_per_week"
                    }
                  }
                }
              }
            ]
          },
          "errors": []
        }

### Poll Status

The following example shows a sample result when user polls the status of a job

    {"status":
        {   "completionTime":"2018-10-19T20:15:08Z",
            "conditions":[{"lastProbeTime":"2018-10-19T20:15:08Z",
                            "lastTransitionTime":"2018-10-19T20:15:08Z",
                            "status":"True",
                            "type":"Complete"
                         }],
            "startTime":"2018-10-19T19:59:53Z",
            "succeeded":1
        }
    }


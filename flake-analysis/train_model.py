import datetime
import time
import csv
import io
import os
import boto3
import uuid
import argparse
import subprocess
import time
import json
import sys
import inspect
currentdir = os.path.dirname(
               os.path.abspath(
                inspect.getfile(inspect.currentframe())
                )
               )
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir + "/storage")
import s3

parser = argparse.ArgumentParser()
parser.add_argument('-s3Path', help='path to training data', default='')
parser.add_argument('-s3endpointUrl', help='s3 endpoint url', default='')
parser.add_argument('-s3objectStoreLocation', help='s3 bucket', default='')
parser.add_argument('-s3accessKey', help='s3 access key', default='')
parser.add_argument('-s3secretKey', help='s3 secret key', default='')
parser.add_argument('-s3Destination', help='path to store results', default='')
args = parser.parse_args()

if os.path.exists("data.jsonl"):
    os.remove("data.jsonl")

# Create jsonl from json training data
fname = 'data.jsonl'
session = s3.create_session_and_resource(args.s3accessKey,
                                         args.s3secretKey,
                                         args.s3endpointUrl)
objects = s3.get_objects(session, args.s3objectStoreLocation, args.s3Path)

for key in objects:
    obj = session.Object(args.s3objectStoreLocation, key)
    contents = obj.get()['Body'].read().decode('utf-8')
    if contents:
        jcontents = json.loads(contents)
        with open(fname, 'a') as outfile:
            json.dump(jcontents, outfile)
            outfile.write('\n')

# Train model
cmd = "python flake-analysis/bots/learn-tests --dry " + fname
os.system(cmd)
print("Training complete!")

cmd = 'find flake-analysis/bots/images/ -name tests-learn*.model -printf "%f"'
model = os.popen(cmd).read()
print("Found model : " + model)

print("Uploading model to: " + args.s3Destination)

# Upload file to the destination folder in the Ceph backend
s3.upload_file(session, args.s3objectStoreLocation,
               "flake-analysis/bots/images/" + model, args.s3Destination)

import datetime
import time
import csv
import io
import os
import uuid
import argparse
import subprocess
import zipfile
import json
import re
import sys
import inspect
currentdir = os.path.dirname(
               os.path.abspath(
                inspect.getfile(inspect.currentframe())
                )
               )
parentdir = os.path.dirname(currentdir)
sys.path.append(parentdir + "/storage")
import s3

parser = argparse.ArgumentParser()
parser.add_argument('-model', help='Path to trailed model', default='')
parser.add_argument('-s3Path', help='path to prediction data set', default='')
parser.add_argument('-s3endpointUrl', help='s3 endpoint url', default='')
parser.add_argument('-s3objectStoreLocation', help='s3 bucket', default='')
parser.add_argument('-s3accessKey', help='s3 access key', default='')
parser.add_argument('-s3secretKey', help='s3 secret key', default='')
parser.add_argument('-s3Destination', help='path to store results', default='')
args = parser.parse_args()

MODEL_PATH = 'flake-analysis/bots/images/'
modelurl = args.model.split("/")
modelfile = modelurl[-1]

# Download the trained model from storage backend in to MODEL_PATH
session = s3.create_session_and_resource(args.s3accessKey,
                                         args.s3secretKey,
                                         args.s3endpointUrl)
s3.download_file(session,
                 args.s3objectStoreLocation,
                 args.model,
                 MODEL_PATH + modelfile)

if not os.path.exists('results'):
    os.makedirs('results')

objects = s3.get_objects(session, args.s3objectStoreLocation, args.s3Path)

for key in objects:
    obj = session.Object(args.s3objectStoreLocation, key)
    contents = obj.get()['Body'].read().decode('utf-8')
    keylist = key.split("/")
    filename = keylist[-1]
    if contents:
        jcontents = json.loads(contents)
        log = jcontents['log']
        label = jcontents['label']
        with open('tmp.txt', 'w') as outfile:
            outfile.write(log)
        cat_file = 'cat tmp.txt '
        pipe_predict = '| python flake-analysis/bots/tests-policy -model '
        get_flakes = '| grep Flake > result.txt ; cat result.txt'
        cmd = cat_file + pipe_predict + modelfile + ' tmp.txt ' + get_flakes
        result = os.popen(cmd).read()
        flake_probability = re.findall("\d+\.\d+", result)
        if flake_probability:
            jcontents['flake'] = flake_probability[0]
        else:
            jcontents['flake'] = 0.0
        filepath = 'results/' + filename
        with open(filepath, 'w') as fp:
            json.dump(jcontents, fp)
            fp.write('\n')

print("Prediction complete!")
# Upload results to storage
s3.upload_folder(args.s3accessKey,
                 args.s3secretKey,
                 args.s3endpointUrl,
                 args.s3objectStoreLocation,
                 'results',
                 args.s3Destination)

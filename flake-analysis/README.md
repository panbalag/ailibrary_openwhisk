# Flake Analysis

Flake analysis is used to identify false positive in test failures. The model uses DBSCAN clustering algorithm to cluster false positives and then predict the chances of a given failure being a false positive.

## Contents

`train_model.py` - Clustering (DBSCAN) based model to group false positives in test failures
##### Parameters
* s3Path - object key containing training data (location in the bucket)
* s3Destination - location to store the trained model (object key including model name)

`predict_flakes.py` - Compute the probability a given test failure is false positive using the above trained model.
##### Parameters
* model - object key including model name (same as s3Destination above unless you relocate the model).
* s3Path - object key containing test failures to run prediction on (location in the bucket)
* s3Destination - location to store the prediction results

## Workflow

### Save Data

#### Training Data

The following example shows sample s3Path that points to the folder where training data is stored.

    s3Path - flake_analysis/datasets/training/records
    Files -
        record1.json
        record2.json
        record3.json
        ..

The following example shows what an individual training data looks like. 

    record1.json
    {
        "id":"041f6832-aa14-4f6e-891d-31aaf8d7ed01",
        "status":"failure",
        "pull":7331,
        "log":"# ----------------------------------------------------------------------\n# testFormatTypes (check_storage_format.TestStorage)\n#\n> Using older 'udisks2' implementation: 2.1.8\nnot ok 26 testFormatTypes (check_storage_format.TestStorage) duration: 78s\nTraceback (most recent call last):\n  File \"/build/cockpit/bots/../test/verify/check-storage-format\", line 74, in testFormatTypes\n    check_type(\"ext4\")\n  File \"/build/cockpit/bots/../test/verify/check-storage-format\", line 71, in check_type\n    self.content_row_wait_in_col(1, 1, type + \" File System\")\n  File \"/build/cockpit/test/verify/storagelib.py\", line 89, in content_row_wait_in_col\n    self.retry(None, lambda: self.browser.is_present(col) and val in self.browser.text(col), None)\n  File \"/build/cockpit/test/verify/storagelib.py\", line 65, in retry\n    b.wait_checkpoint()\n  File \"/build/cockpit/test/common/testlib.py\", line 291, in wait_checkpoint\n    return self.phantom.wait_checkpoint()\n  File \"/build/cockpit/test/common/testlib.py\", line 821, in <lambda>\n    return lambda *args: self._invoke(name, *args)\n  File \"/build/cockpit/test/common/testlib.py\", line 847, in _invoke\n    raise Error(res['error'])\nError: timeout happened\nWrote TestStorage-testFormatTypes-debian-testing-127.0.0.2-2801-FAIL.png\nWrote TestStorage-testFormatTypes-debian-testing-127.0.0.2-2801-FAIL.html\nWrote TestStorage-testFormatTypes-debian-testing-127.0.0.2-2801-FAIL.js.log\nJournal extracted to TestStorage-testFormatTypes-debian-testing-127.0.0.2-2801-FAIL.log\n",
        "test":"testFormatTypes (check_storage_format.TestStorage)",
        "context":"verify/debian-testing",
        "date":"2017-07-19T11:56:01Z",
        "merged":true,
        "revision":"b32635869b9e87cdd9e42b6e6123150d500f6862"
    }

    
#### Prediction Data

The following example shows sample s3Path that points to the folder where prediction data is stored.

    s3Path - flake_analysis/prediction-data/failures/records
    Files -
        testfailure1.json
        testfailure2.json
        testfailure3.json
        ..

The following example shows what an individual training data looks like. 

    testfailure1.json
    {
        "id":"e54bb558-f134-4356-b2f0-59c5cdf44ed3",
        "label":"failure2.txt",
        "log":"# testBasic (check_journal.TestJournal)\n#\n[0905/212816.673168:ERROR:gpu_process_transport_factory.cc(1017)] Lost UI shared context.\n\nDevTools listening on ws://127.0.0.1:9678/devtools/browser/faa4899a-aec5-4a63-8717-9c991913b26b\n[0905/212816.716594:ERROR:zygote_host_impl_linux.cc(267)] Failed to adjust OOM score of renderer with pid 42364: Permission denied (13)\n[0905/212816.874362:ERROR:zygote_host_impl_linux.cc(267)] Failed to adjust OOM score of renderer with pid 42409: Permission denied (13)\nWarning: Stopping systemd-journald.service, but it can still be activated by:\n  systemd-journald-audit.socket\n  systemd-journald.socket\n  systemd-journald-dev-log.socket\n> warning: transport closed: disconnected\nssh_exchange_identification: read: Connection reset by peer\nCDP: {\"source\":\"network\",\"level\":\"error\",\"text\":\"Failed to load resource: net::ERR_EMPTY_RESPONSE\",\"timestamp\":1536182906007.98,\"url\":\"http://127.0.0.2:9591/cockpit/static/fonts/OpenSans-Light-webfont.woff\",\"networkRequestId\":\"42409.28\"}\nssh_exchange_identification: read: Connection reset by peer\n> log: transport closed, dropped message:  1:2!6\n{\"call\":[\"/org/freedesktop/timedate1\",\"org.freedesktop.DBus.Properties\",\"Get\",[\"org.freedesktop.timedate1\",\"NTPSynchronized\"]],\"id\":\"4\"}\n> log: transport closed, dropped message:  1:2!6\n{\"call\":[\"/org/freedesktop/timedate1\",\"org.freedesktop.DBus.Properties\",\"Get\",[\"org.freedesktop.timedate1\",\"NTPSynchronized\"]],\"id\":\"5\"}\nssh_exchange_identification: read: Connection reset by peer\nssh_exchange_identification: read: Connection reset by peer\nssh_exchange_identification: read: Connection reset by peer\nssh_exchange_identification: read: Connection reset by peer\n> log: transport closed, dropped message:  1:2!6\n{\"call\":[\"/org/freedesktop/timedate1\",\"org.freedesktop.DBus.Properties\",\"Get\",[\"org.freedesktop.timedate1\",\"NTPSynchronized\"]],\"id\":\"6\"}\nok 42 testBasic (check_journal.TestJournal) # duration: 43s\n"
    }

### Run Model

#### Training

    curl -u user:secret \
    "https://openwhisk-dh-prod-ow.openshift.com/api/v1/namespaces/_/actions/ai-library/flake-analysis-training?" \
    -X POST -H "Content-Type: application/json" -d \
    '{ "name"               : "flake-analysis-training", 
       "app_args": "-s3Path=flake_analysis/datasets/training/records -s3Destination=flake_analysis/models/testflakes.model"}'

#### Prediction

    curl -u user:secret \
    "https://openwhisk-dh-prod-ow.openshift.com/api/v1/namespaces/_/actions/ai-library/flake-analysis-prediction?" \
    -X POST -H "Content-Type: application/json" -d \
    '{ "name"               : "flake-analysis-prediction", 
      "app_args": "-model=flake_analysis/models/testflakes.model -s3Path=flake_analysis/prediction-data/failures/records -s3Destination=flake_analysis/predictions"}'
      
#### Poll Status

    curl -u user:secret \
    "https://openwhisk-dh-prod-ow.openshift.com/api/v1/namespaces/_/actions/ai-library/poll-status?" \
    -X POST -H "Content-Type: application/json" -d \
    '{ "name"               : "flake-analysis-training"}'

    curl -u user:secret \
    "https://openwhisk-dh-prod-ow.openshift.com/api/v1/namespaces/_/actions/ai-library/poll-status?" \
    -X POST -H "Content-Type: application/json" -d \
    '{ "name"               : "flake-analysis-prediction"}'


### Use Results

#### Training

The following example shows sample s3Destination that points to where the trained model is stored.

    s3Destination - flake_analysis/models/testflakes.model
    File -
        testflakes.model
    
#### Prediction

The following example shows sample s3Destination that points to the folder where prediction results are stored.

    s3Destination - flake_analysis/predictions
    Files -
        testfailure1.json
        testfailure2.json
        testfailure3.json
        ..

The following example shows what an individual prediction result looks like. The field 'flake' shows the probability of the given test failure being a false positive.

    testfailure1.json
    {
        "id":"e54bb558-f134-4356-b2f0-59c5cdf44ed3",
        "label":"failure2.txt",
        "log":"# testBasic (check_journal.TestJournal)\n#\n[0905/212816.673168:ERROR:gpu_process_transport_factory.cc(1017)] Lost UI shared context.\n\nDevTools listening on ws://127.0.0.1:9678/devtools/browser/faa4899a-aec5-4a63-8717-9c991913b26b\n[0905/212816.716594:ERROR:zygote_host_impl_linux.cc(267)] Failed to adjust OOM score of renderer with pid 42364: Permission denied (13)\n[0905/212816.874362:ERROR:zygote_host_impl_linux.cc(267)] Failed to adjust OOM score of renderer with pid 42409: Permission denied (13)\nWarning: Stopping systemd-journald.service, but it can still be activated by:\n  systemd-journald-audit.socket\n  systemd-journald.socket\n  systemd-journald-dev-log.socket\n> warning: transport closed: disconnected\nssh_exchange_identification: read: Connection reset by peer\nCDP: {\"source\":\"network\",\"level\":\"error\",\"text\":\"Failed to load resource: net::ERR_EMPTY_RESPONSE\",\"timestamp\":1536182906007.98,\"url\":\"http://127.0.0.2:9591/cockpit/static/fonts/OpenSans-Light-webfont.woff\",\"networkRequestId\":\"42409.28\"}\nssh_exchange_identification: read: Connection reset by peer\n> log: transport closed, dropped message:  1:2!6\n{\"call\":[\"/org/freedesktop/timedate1\",\"org.freedesktop.DBus.Properties\",\"Get\",[\"org.freedesktop.timedate1\",\"NTPSynchronized\"]],\"id\":\"4\"}\n> log: transport closed, dropped message:  1:2!6\n{\"call\":[\"/org/freedesktop/timedate1\",\"org.freedesktop.DBus.Properties\",\"Get\",[\"org.freedesktop.timedate1\",\"NTPSynchronized\"]],\"id\":\"5\"}\nssh_exchange_identification: read: Connection reset by peer\nssh_exchange_identification: read: Connection reset by peer\nssh_exchange_identification: read: Connection reset by peer\nssh_exchange_identification: read: Connection reset by peer\n> log: transport closed, dropped message:  1:2!6\n{\"call\":[\"/org/freedesktop/timedate1\",\"org.freedesktop.DBus.Properties\",\"Get\",[\"org.freedesktop.timedate1\",\"NTPSynchronized\"]],\"id\":\"6\"}\nok 42 testBasic (check_journal.TestJournal) # duration: 43s\n",
        "flake": "0.667"
    }

#### Poll Status

The following example shows a sample result when user polls the status of a job

    {"status":
        {   "completionTime":"2018-10-19T20:15:08Z",
            "conditions":[{"lastProbeTime":"2018-10-19T20:15:08Z",
                            "lastTransitionTime":"2018-10-19T20:15:08Z",
                            "status":"True",
                            "type":"Complete"
                         }],
            "startTime":"2018-10-19T19:59:53Z",
            "succeeded":1
        }
    }

